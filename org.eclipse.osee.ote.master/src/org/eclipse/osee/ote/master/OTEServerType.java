package org.eclipse.osee.ote.master;

public interface OTEServerType {
   String getName();
}
