/*********************************************************************
 * Copyright (c) 2004, 2007 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.ote.test.manager.pages;

/**
 * @author Roberto E. Escobar
 */
public class StorageKeys {

   public static final String DEBUG_OPTIONS = "debug.options";
   public static final String DISTRIBUTION_STATEMENT = "distrobution.statement";
   public static final String FORMAL_TEST_TYPE = "formal.test.type";
   public static final String DEMONSTRATION_NOTES = "demo.notes";
   public static final String DEMONSTRATION_BUILD = "demo.build";
   public static final String DEMONSTRATION_WITNESSES = "demo.witnesses";
   public static final String DEMONSTRATION_EXECUTED_BY = "demo.executed.by";
   public static final String SERVER_OUTPUT_SELECTION_STORAGE_KEY = "server.output.selection";
   public static final String SERVER_OUTPUT_FILE_PATH_STORAGE_KEY = "server.output.file.path";

   private StorageKeys() {
   }
}
