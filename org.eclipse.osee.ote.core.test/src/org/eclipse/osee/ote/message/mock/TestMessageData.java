/*********************************************************************
 * Copyright (c) 2004, 2007 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.osee.ote.message.mock;

import org.eclipse.osee.ote.message.IMessageHeader;
import org.eclipse.osee.ote.message.data.IMessageDataVisitor;
import org.eclipse.osee.ote.message.data.MessageData;
import org.eclipse.osee.ote.message.enums.DataType;

public class TestMessageData extends MessageData {

   public TestMessageData(String typeName, String name, int dataByteSize, int offset, DataType memType) {
      super(typeName, name, dataByteSize, offset, memType);
   }

   @Override
   public IMessageHeader getMsgHeader() {
      return null;
   }

   @Override
   public void initializeDefaultHeaderValues() {

   }

   @Override
   public void visit(IMessageDataVisitor visitor) {

   }

   @Override
   public void zeroize() {

   }

}
