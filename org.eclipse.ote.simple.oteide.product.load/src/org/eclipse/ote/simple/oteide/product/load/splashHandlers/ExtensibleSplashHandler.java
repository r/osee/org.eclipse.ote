/*********************************************************************
 * Copyright (c) 2019 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.ote.simple.oteide.product.load.splashHandlers;

import org.eclipse.ui.splash.BasicSplashHandler;

/**
 * @author Andrew M. Finkbeiner
 */
public class ExtensibleSplashHandler extends BasicSplashHandler {

   public ExtensibleSplashHandler() {
      // Intentionally empty block
   }

}
