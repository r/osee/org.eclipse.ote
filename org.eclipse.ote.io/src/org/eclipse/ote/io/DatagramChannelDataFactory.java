package org.eclipse.ote.io;

public interface DatagramChannelDataFactory {

   public DatagramChannelData create(DatagramChannelDataPool datagramChannelDataPool);

}
