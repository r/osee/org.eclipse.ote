/*********************************************************************
 * Copyright (c) 2009 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.osee.framework.ui.workspacebundleloader.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * @author Ryan D. Brooks
 */
public class Activator implements BundleActivator {

   public static final String BUNDLE_ID = "org.eclipse.osee.framework.ui.workspacebundleloader";

   @Override
   public void start(BundleContext context) throws Exception {
   }

   @Override
   public void stop(BundleContext context) throws Exception {
   }

}
