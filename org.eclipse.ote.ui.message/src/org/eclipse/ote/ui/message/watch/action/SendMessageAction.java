/*********************************************************************
 * Copyright (c) 2010 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.ote.ui.message.watch.action;

import java.util.logging.Level;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.osee.framework.logging.OseeLog;
import org.eclipse.osee.framework.ui.swt.Displays;
import org.eclipse.osee.ote.message.tool.MessageMode;
import org.eclipse.ote.ui.message.tree.WatchedMessageNode;

/**
 * @author Ken J. Aguilar
 */
public class SendMessageAction extends Action {
   private final WatchedMessageNode msgNode;

   public SendMessageAction(WatchedMessageNode msgNode) {
      super("Send");
      this.msgNode = msgNode;
      setEnabled(msgNode.isEnabled() && msgNode.getSubscription().getMessageMode() == MessageMode.WRITER && msgNode.getSubscription().isActive());

   }

   @Override
   public void run() {
      try {
         msgNode.getSubscription().send();
      } catch (Exception e) {
         String message = "could not send the message " + msgNode.getMessageClassName();
         OseeLog.log(SendMessageAction.class, Level.SEVERE, message, e);
         MessageDialog.openError(Displays.getActiveShell(), "Send Message", message + ". Check error log for trace");
      }
   }

}
