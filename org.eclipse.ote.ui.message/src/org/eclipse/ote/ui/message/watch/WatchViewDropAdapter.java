/*********************************************************************
 * Copyright (c) 2011 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.ote.ui.message.watch;

import java.io.IOException;
import java.util.logging.Level;
import org.eclipse.osee.framework.jdk.core.util.Lib;
import org.eclipse.osee.framework.logging.OseeLog;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.DropTargetListener;

public class WatchViewDropAdapter implements DropTargetListener {

	private WatchView watchViewer;
	
	public WatchViewDropAdapter(WatchView watchViewer) {
		this.watchViewer = watchViewer;
	}

	@Override
	public void dragEnter(DropTargetEvent event) {
	}

	@Override
	public void dragLeave(DropTargetEvent event) {
	}

	@Override
	public void dragOperationChanged(DropTargetEvent event) {
	}

	@Override
	public void dragOver(DropTargetEvent event) {
	}

	@Override
	public void drop(DropTargetEvent event) {
		if(event.data instanceof String[]){
			for(String file: (String[])event.data){
				java.io.File realFile = new java.io.File(file);
				if(realFile.exists() && !realFile.isDirectory()){
					try {
						String fileAsString = Lib.fileToString(realFile);
						SignalStripper signalStripper = new SignalStripper();
						AddWatchParameter watchParam = new AddWatchParameter();
						signalStripper.generateStringToWrite(fileAsString, watchParam);
						watchViewer.addWatchMessage(watchParam);
					} catch (IOException e) {
						OseeLog.log(WatchViewDropAdapter.class, Level.SEVERE, "Failed to read file from drag and drop into message watch.", e);
					}
				}
			}
		}
	}

	@Override
	public void dropAccept(DropTargetEvent event) {
	}

}
