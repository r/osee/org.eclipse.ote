/*********************************************************************
 * Copyright (c) 2010 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.ote.ui.message.watch.action;

import java.util.List;
import java.util.logging.Level;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.osee.framework.logging.OseeLog;
import org.eclipse.osee.framework.ui.swt.Displays;
import org.eclipse.osee.ote.message.Message;
import org.eclipse.osee.ote.message.elements.Element;
import org.eclipse.osee.ote.message.elements.EnumeratedElement;
import org.eclipse.osee.ote.message.tool.MessageMode;
import org.eclipse.ote.ui.message.internal.Activator;
import org.eclipse.ote.ui.message.tree.ElementNode;
import org.eclipse.ote.ui.message.tree.WatchedMessageNode;
import org.eclipse.ote.ui.message.watch.WatchView;
import org.eclipse.ui.dialogs.ListDialog;

/**
 * @author Ken J. Aguilar
 */
public class SetValueAction extends Action {
   private final WatchedMessageNode msgNode;
   private final ElementNode node;
   private final WatchView watchView;
   
   public SetValueAction(WatchView watchView, ElementNode node) {
      super("Set Value");
      this.watchView = watchView;
      this.node = node;
      msgNode = (WatchedMessageNode) node.getMessageNode();
      setEnabled(node.isEnabled() && msgNode.getSubscription().getMessageMode() == MessageMode.WRITER && msgNode.getSubscription().isActive());
   }

   @Override
   public void run() {
     Message msg = msgNode.getSubscription().getMessage();
      List<Object> path = node.getElementPath().getElementPath();
      Element element = msg.getElement(path);
      if (element instanceof EnumeratedElement<?>) {
         final EnumeratedElement<?> enumElement = (EnumeratedElement<?>) element;
         try {
            final Enum<?>[] values = enumElement.getEnumValues();
            int width = 0;
            for (Enum<?> val : values) {
               width = val.toString().length() > width ? val.toString().length() : width;
            }

            final ListDialog dialog = new ListDialog(Displays.getActiveShell());
            dialog.setInput(values);
            dialog.setTitle("Set Value");
            dialog.setAddCancelButton(true);
            dialog.setWidthInChars(width + 5);
            dialog.setMessage(element.getFullName() + "\nSelect New Value");
            dialog.setContentProvider(new ITreeContentProvider() {

               @Override
               public Object[] getChildren(Object parentElement) {
                  return null;
               }

               @Override
               public Object getParent(Object element) {
                  return values;
               }

               @Override
               public boolean hasChildren(Object element) {
                  return false;
               }

               @Override
               public Object[] getElements(Object inputElement) {
                  return values;
               }

               @Override
               public void dispose() {
               }

               @Override
               public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
               }

            });

            dialog.setLabelProvider(new LabelProvider() {
               @Override
               public boolean isLabelProperty(Object element, String property) {
                  return false;
               }
            });

            int result = dialog.open();
            if (result == Window.OK) {
               final Object[] objs = dialog.getResult();
               if (objs.length == 1) {
                  final String value = ((Enum<?>) objs[0]).name();
                  msgNode.getSubscription().setElementValue(path, value);
               }
            }
         } catch (Throwable t) {
            final String logMsg =
               String.format("Exception while attempting to set element %s of message %s", element.getName(),
                  msg.getName());
            OseeLog.log(Activator.class, Level.SEVERE, logMsg, t);
         }

      } else {
         InputDialog dialog =
            new InputDialog(Displays.getActiveShell(), "Set " + element.getFullName(), "Enter set value", "0",
               new IInputValidator() {

                  @Override
                  public String isValid(String newText) {
                     // No Error accept all values;
                     return null;
                  }
               });

         //         final EntryDialog dialog =
         //               new EntryDialog(Displays.getActiveShell(), "Set " + element.getFullName(), null,
         //                     "Enter set value", MessageDialog.QUESTION, new String[] {"Ok", "Cancel"}, 0);
         if (dialog.open() == 0) {
            try {
               final String val = dialog.getValue();
               msgNode.getSubscription().setElementValue(path, val);
               watchView.saveWatchFile();
            } catch (Exception ex) {
               OseeLog.logf(Activator.class, Level.SEVERE, ex, "Unable to set the %s element for the message %s",
                  element.getName(), msg.getName());
               MessageDialog.openError(Displays.getActiveShell(), "Error", "Could not set value");
            }
         }
      }
   }
}
