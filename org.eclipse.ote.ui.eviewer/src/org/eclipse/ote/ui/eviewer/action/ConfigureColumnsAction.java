/*********************************************************************
 * Copyright (c) 2013 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.ote.ui.eviewer.action;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.window.Window;
import org.eclipse.ote.ui.eviewer.Activator;
import org.eclipse.ote.ui.eviewer.view.ColumnConfiguration;
import org.eclipse.ote.ui.eviewer.view.ColumnConfigurationDialog;
import org.eclipse.ote.ui.eviewer.view.ElementContentProvider;
import org.eclipse.ote.ui.eviewer.view.MessageDialogs;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

/**
 * @author Ken J. Aguilar
 */
public class ConfigureColumnsAction extends Action {

   private final ElementContentProvider provider;

   public ConfigureColumnsAction(ElementContentProvider provider) {
      super("Configure Columns", IAction.AS_PUSH_BUTTON);
      setImageDescriptor(Activator.getImageDescriptor("OSEE-INF/images/table_config.gif"));
      this.provider = provider;
   }

   @Override
   public void run() {
      Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
      ColumnConfiguration configuration = new ColumnConfiguration(provider);
      ColumnConfigurationDialog dialog = new ColumnConfigurationDialog(shell, configuration);
      if (dialog.open() == Window.OK) {
         configuration.apply(provider);
         if (!provider.updateInternalFile()) {
            MessageDialogs.saveColumnFileFail(shell);
         }

      }
   }
}
