package org.eclipse.ote.ui.eviewer.tohex;

import org.eclipse.osee.ote.message.elements.DiscreteElement;

public interface IToHex {
   public String toHex(DiscreteElement<?> element);
}
