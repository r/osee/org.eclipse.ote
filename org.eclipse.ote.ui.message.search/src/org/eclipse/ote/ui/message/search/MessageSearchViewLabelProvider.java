/*********************************************************************
 * Copyright (c) 2013 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.ote.ui.message.search;

import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.ote.message.lookup.MessageInputItem;
import org.eclipse.swt.graphics.Image;

public class MessageSearchViewLabelProvider extends StyledCellLabelProvider {

   private static final Image elementImg = OteMessageViewImage.PIPE.getImage();
   private static final Image messageImg = OteMessageViewImage.GEAR.getImage();

   @Override
   public void update(ViewerCell cell) {
      Object element = cell.getElement();
      if(element instanceof MessageInputItem){
         MessageInputItem item = (MessageInputItem)element;
         cell.setImage(getImage(element));
         StyledString text = new StyledString();
         text.append(item.getName());
         if(item.getType().length() > 0){
            text.append("  [" +item.getType() + "] ", StyledString.QUALIFIER_STYLER);
         }
         cell.setText(text.toString());
         cell.setStyleRanges(text.getStyleRanges());
      } else {
         cell.setText(getText(cell.getElement()));
      }
      super.update(cell);
   }

   private String getText(Object element) {
      if(element instanceof MessageInputItem){
         return ((MessageInputItem)element).getName();
      }
      return element.toString();
   }

   private Image getImage(Object element) {
      if(element instanceof MessageInputItem){
         if(((MessageInputItem)element).getElementPath() == null){
            return messageImg;
         } else {
            return elementImg;
         }
      }
      return null;
   }
}
