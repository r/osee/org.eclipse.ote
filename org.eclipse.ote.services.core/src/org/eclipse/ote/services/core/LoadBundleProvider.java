package org.eclipse.ote.services.core;

import java.util.List;

public interface LoadBundleProvider {
   List<String> getBundleSymbolicNames();
}
