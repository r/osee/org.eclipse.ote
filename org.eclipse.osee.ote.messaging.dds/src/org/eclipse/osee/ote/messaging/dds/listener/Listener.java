/*********************************************************************
 * Copyright (c) 2004, 2007 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.osee.ote.messaging.dds.listener;

/**
 * This is the base interface for all of the DDS listener interfaces. The sole purpose of this interface is to provide a
 * common root for all of the DDS listener interfaces.
 * 
 * @author Robert A. Fisher
 * @author David Diepenbrock
 */
public interface Listener {

}
