/*********************************************************************
 * Copyright (c) 2004, 2007 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.osee.ote.client.msg.core.db;

import java.net.InetSocketAddress;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.osee.ote.client.msg.core.internal.MessageServiceSupport;
import org.eclipse.osee.ote.message.Message;
import org.eclipse.osee.ote.message.commands.SubscribeToMessage;
import org.eclipse.osee.ote.message.commands.UnSubscribeToMessage;
import org.eclipse.osee.ote.message.enums.DataType;
import org.eclipse.osee.ote.message.interfaces.IMsgToolServiceClient;
import org.eclipse.osee.ote.message.tool.MessageMode;
import org.eclipse.osee.ote.message.tool.SubscriptionDetails;
import org.eclipse.osee.ote.message.tool.SubscriptionKey;

/**
 * @author Ken J. Aguilar
 */
public class MessageInstance {

   private final DataType type;
   private final MessageMode mode;
   private final Message msg;
   private SubscriptionKey serverSubscriptionKey = null;
   private int refcount = 0;
   private boolean supported = true;
   private volatile boolean connected = false;
   private HashSet<DataType> availableTypes = new HashSet<>();

   public MessageInstance(Message msg, MessageMode mode, DataType type) {
      this.msg = msg;
      this.mode = mode;
      this.type = type;
   }

   public Message getMessage() {
      return msg;
   }

   public SubscriptionKey getServerSubscriptionKey() {
      return serverSubscriptionKey;
   }

   public void setServerSubscriptionKey(SubscriptionKey serverSubscriptionKey) {
      this.serverSubscriptionKey = serverSubscriptionKey;
   }

   public boolean isAttached() {
      return serverSubscriptionKey != null;
   }

   public Integer attachToService(IMsgToolServiceClient client) throws Exception {
      InetSocketAddress address = client.getAddressByType(msg.getClass().getName(), type);
      SubscriptionDetails details;
      if(address == null){
         details = null;
      } else {
         details = MessageServiceSupport.subscribeToMessage(new SubscribeToMessage(msg.getClass().getName(), type, mode,
               client.getAddressByType(msg.getClass().getName(), type), client.getTestSessionKey()));
         
      }
      if (details == null) {
         supported = false;
         return null;
      }
      supported = true;
      msg.setData(details.getCurrentData());
      connected = true;
      //determine types for message
      Set<? extends DataType> envSet = MessageServiceSupport.getAvailablePhysicalTypes();
      Set<DataType> available = msg.getAssociatedMessages().keySet();
      for(DataType type : available.toArray(new DataType[available.size()])){
         if(envSet.contains(type)){
            availableTypes.add(type);
         }
      }
      
      serverSubscriptionKey = details.getKey();
      return serverSubscriptionKey.getId();
   }

   public void detachService(IMsgToolServiceClient client) throws Exception {
      if (supported) {
         MessageServiceSupport.unsubscribeToMessage(new UnSubscribeToMessage(msg.getClass().getName(), mode, type,
            client.getAddressByType(msg.getClass().getName(), type)));
      }
      availableTypes.clear();
      connected = false;
      serverSubscriptionKey = null;
   }

   public Integer getId() {
      return serverSubscriptionKey != null ? serverSubscriptionKey.getId() : null;
   }

   public void incrementReferenceCount() {
      refcount++;
   }

   public void decrementReferenceCount() {
      refcount--;
   }

   public boolean hasReferences() {
      return refcount > 0;
   }

   public DataType getType() {
      return type;
   }

   public MessageMode getMode() {
      return mode;
   }

   public Set<DataType> getAvailableTypes() {
     return availableTypes;
   }

   public boolean isSupported() {
      return supported;
   }

   @Override
   public String toString() {
      return String.format("Message Instance(type=%s, mode=%s, ref=%d, supported=%b)", type.name(), mode.name(),
         refcount, supported);
   }
}
