/*********************************************************************
 * Copyright (c) 2004, 2007 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.osee.ote.ui.test.manager.pages.contributions;


/**
 * @author Roberto E. Escobar
 */
public class TestManagerStorageKeys {

   public static final String BATCH_MODE_ENABLED_KEY = "is.batch.mode.enabled";
   public static final String KEEP_OLD_OUTFILE_COPIES_ENABLED_KEY = "is.keep.old.outfile.copies.enabled";
   public static final String ABORT_ON_FAIL_KEY = "is.abort.mode.enabled";
   public static final String PAUSE_ON_FAIL_KEY = "is.pause.mode.enabled";
   public static final String PRINT_FAIL_TO_CONSOLE = "is.print.fail.enabled";
   public static final String LOGGING_LEVEL_KEY = "logging.level";
   public static final String SCRIPT_OUTPUT_DIRECTORY_KEY = "script.output.directory";

   private TestManagerStorageKeys() {
   }
}
