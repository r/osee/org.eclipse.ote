/*********************************************************************
 * Copyright (c) 2004, 2007 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.osee.ote.ui.test.manager.configuration.pages;

public interface ScriptPageConstants {

   public static final String RAW_FILENAME_FIELD = "Path";
   public static final String RESULT_FIELD = "Result";
   public static final String RUNNABLE_FIELD = "IsRunnable";
   public static final String SCRIPT_ENTRY = "ScriptEntry";
   public static final String SCRIPT_NAME_FIELD = "Name";
   public static final String SCRIPTPAGE_CONFIG = "ScriptPageConfig";
   public static final String SERVICES_ENTRY = "ServicesSettings";
   public static final String STATUS_FIELD = "Status";
   public static final String CLASS_NAME = "ClassName";
   public static final String CLASS_PATH = "ClassPath";
}
