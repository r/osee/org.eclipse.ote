package org.eclipse.osee.ote;


public enum HostServerProperties {
   name,
   station,
   version,
   type,
   maxUsers,
   comment,
   date,
   group,
   owner,
   id,
   activeMq,
   appServerURI,
   oteUdpEndpoint,
   serverLaunchWorkingDir,
   isSim
}
