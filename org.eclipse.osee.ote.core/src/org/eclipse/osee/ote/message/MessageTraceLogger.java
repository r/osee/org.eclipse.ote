package org.eclipse.osee.ote.message;

/**
 * The {@code MessageTraceDbLogger} interface is a logger interface.
 * 
 * @author Andy Jury
 *
 */
public interface MessageTraceLogger {

   public void logMessageTraceOutput(MessageTraceOutput messageTraceOutput);
}
