/*********************************************************************
 * Copyright (c) 2004, 2007 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.osee.ote.message.interfaces;

import java.util.Collection;
import java.util.Set;

import org.eclipse.osee.framework.jdk.core.type.Pair;
import org.eclipse.osee.ote.core.TestException;
import org.eclipse.osee.ote.message.Message;
import org.eclipse.osee.ote.message.enums.DataType;
import org.eclipse.osee.ote.message.listener.DDSDomainParticipantListener;
import org.eclipse.osee.ote.message.listener.IMessageCreationListener;
import org.eclipse.osee.ote.messaging.dds.entity.Publisher;

/**
 * @author Andrew M. Finkbeiner
 */
public interface IMessageManager<U extends Message> {
   void destroy();

   <CLASSTYPE extends U> CLASSTYPE createMessage(Class<CLASSTYPE> messageClass) throws TestException;

   <CLASSTYPE extends U> int getReferenceCount(CLASSTYPE classtype);

   <CLASSTYPE extends U> CLASSTYPE findInstance(Class<CLASSTYPE> clazz, boolean writer);

   Collection<U> getAllMessages();

   Collection<U> getAllReaders();

   Collection<U> getAllWriters();

   Collection<U> getAllReaders(DataType type);

   Collection<U> getAllWriters(DataType type);

   void init() throws Exception;

   boolean isPhysicalTypeAvailable(DataType physicalType);

   IMessageRequestor<U> createMessageRequestor(String name);

   Class<? extends U> getMessageClass(String msgClass) throws ClassCastException, ClassNotFoundException;

   DDSDomainParticipantListener getDDSListener(); 
   
   void addPostCreateMessageListener(IMessageCreationListener<U> listener);

   void addPreCreateMessageListener(IMessageCreationListener<U> listener);

   void addInstanceRequestListener(IMessageCreationListener<U> listener);

   <CLASSTYPE extends U> CLASSTYPE createAndSetUpMessage(Class<CLASSTYPE> messageClass, IMessageRequestor<U> requestor,
         boolean writer) throws TestException;

   Set<DataType> getAvailableDataTypes();

   boolean removeRequestorReference(IMessageRequestor<U> requestor, U msg);

   <CLASSTYPE extends U> CLASSTYPE getMessageWriter(IMessageRequestor<U> messageRequestor, Class<CLASSTYPE> type);
   <CLASSTYPE extends U> CLASSTYPE getMessageReader(IMessageRequestor<U> messageRequestor, Class<CLASSTYPE> type);

   Publisher getPublisher();

   void putMessageDataLookup(Namespace namespace, MessageDataLookup lookup);

   MessageDataLookup getMessageDataLookup(Namespace namespace);

   Set<Pair<Double, Integer>> getTasks();
}
