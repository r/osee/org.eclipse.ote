/*********************************************************************
 * Copyright (c) 2004, 2007 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.osee.ote.core.enums;

/**
 * @author Ryan D. Brooks
 */
public class SkynetTestTypes {
   public static final String EXECUTION_DATE = "Execution Date";
   public static final String TEST_STATUS = "Test Status";
   public static final String TEST_RUN = "Test Run";
   public static final String EXEC5UTION_DATE = "Execution Date";
}
