/*********************************************************************
 * Copyright (c) 2004, 2007 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.osee.ote.core;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import org.eclipse.osee.framework.logging.IHealthStatus;

public class TestException extends RuntimeException {

   private static final long serialVersionUID = -5628986844200418864L;

   private final String threadCauseName;
   private final Level level;
   private List<IHealthStatus> status = new ArrayList<>();

   public TestException(String message, Level level) {
      this(message, level, null);
   }

   public TestException(String message, Level level, Throwable cause) {
      super(message, cause);
      this.level = level;
      threadCauseName = Thread.currentThread().getName();
   }

   public TestException(String message, List<IHealthStatus> status) {
      this(message, Level.SEVERE);
      this.status = status;
   }

   public String getThreadName() {
      return threadCauseName;
   }

   public Level getLevel() {
      return level;
   }

   public List<IHealthStatus> getHealthStatus() {
      return status;
   }
}
