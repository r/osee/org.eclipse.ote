/*********************************************************************
 * Copyright (c) 2004, 2007 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.osee.ote.core.framework;

import java.util.logging.Level;
import org.eclipse.osee.framework.jdk.core.type.IPropertyStore;
import org.eclipse.osee.framework.jdk.core.util.Conditions;
import org.eclipse.osee.framework.logging.BaseStatus;
import org.eclipse.osee.framework.logging.IHealthStatus;
import org.eclipse.osee.framework.logging.OseeLog;
import org.eclipse.osee.ote.core.TestScript;
import org.eclipse.osee.ote.core.environment.TestEnvironment;
import org.eclipse.osee.ote.core.framework.testrun.ITestResultCollector;
import org.eclipse.osee.ote.core.framework.testrun.ITestResultCollectorFactory;
import org.eclipse.osee.ote.core.framework.testrun.ITestRunManager;

public class BaseRunManager implements IRunManager {

   private final ITestRunManager testRunManager;
   private final ITestLifecycleListenerProvider lifecycleListenerProvider;
   private final ITestResultCollectorFactory resultCollectorFactory;
   private ITestResultCollector resultCollector;

   public BaseRunManager(ITestRunManager testRunManager, ITestLifecycleListenerProvider lifecycleListenerProvider, ITestResultCollectorFactory resultCollectorFactory) {
      this.lifecycleListenerProvider = lifecycleListenerProvider;
      this.testRunManager = testRunManager;
      this.resultCollectorFactory = resultCollectorFactory;
   }

   @Override
   public boolean addListener(ITestLifecycleListener listener) {
      return lifecycleListenerProvider.addListener(listener);
   }

   @Override
   public void clearAllListeners() {
      lifecycleListenerProvider.clear();
   }

   @Override
   public boolean removeListener(ITestLifecycleListener listener) {
      return lifecycleListenerProvider.removeListener(listener);
   }

   @Override
   public IMethodResult run(TestEnvironment env, IPropertyStore propertyStore) {
      try {
         this.resultCollector = resultCollectorFactory.createCollector();
         this.resultCollector.initialize(propertyStore, env);
         ResultBuilder rb = new ResultBuilder(true);
         rb.append(lifecycleListenerProvider.notifyPreInstantiation(propertyStore, env));
         if (rb.isReturnStatusOK()) {
            rb.append(testRunManager.initialize(env, propertyStore));
            if (rb.isReturnStatusOK()) {
               try {
                  rb.append(
                     lifecycleListenerProvider.notifyPostInstantiation(propertyStore, testRunManager.getTest(), env));
               } catch (Throwable th) {
                  MethodResultImpl result = new MethodResultImpl(ReturnCode.ERROR);
                  result.addStatus(new BaseStatus(this.getClass().getName(), Level.SEVERE, th));
                  rb.append(result);
               }
               if (rb.isReturnStatusOK()) {
                  try {
                     rb.append(testRunManager.run(propertyStore, env));
                  } catch (Throwable th) {
                     MethodResultImpl result = new MethodResultImpl(ReturnCode.ERROR);
                     result.addStatus(new BaseStatus(this.getClass().getName(), Level.SEVERE, th));
                     rb.append(result);
                  }
               } else {
                  TestScript test = testRunManager.getTest();
                  boolean aborted = false;
                  if (Conditions.hasValues(rb.get().getStatus())) {
                     for (IHealthStatus hs : rb.get().getStatus()) {
                        if (hs.getException() != null) {
                           test.abortDueToThrowable(hs.getException());
                           aborted = true;
                           break;
                        }
                     }
                  }
                  if (!aborted) {
                     test.abort();
                  }
               }
            }
            rb.append(lifecycleListenerProvider.notifyPreDispose(propertyStore, testRunManager.getTest(), env));
            rb.append(testRunManager.dispose());
            rb.append(lifecycleListenerProvider.notifyPostDispose(propertyStore, env));
         }
         this.resultCollector.dispose(env);
         this.resultCollector = null;
         if (!rb.isReturnStatusOK()) {
            OseeLog.log(BaseRunManager.class, Level.SEVERE, rb.toString());
         }
         return rb.get();
      } catch (Throwable th) {
         th.printStackTrace();
         MethodResultImpl status = new MethodResultImpl(ReturnCode.ERROR);
         status.addStatus(new BaseStatus(BaseRunManager.class.getName(), Level.SEVERE, th));
         return status;
      }
   }

   @Override
   public boolean abort() {
      return testRunManager.abort();
   }

   @Override
   public boolean abort(Throwable th, boolean wait) {
      return testRunManager.abort(th, wait);
   }

   @Override
   public boolean isAborted() {
      return testRunManager.isAborted();
   }

   @Override
   public TestScript getCurrentScript() {
      return testRunManager.getTest();
   }

   @Override
   public boolean isCurrentThreadScript() {
      return testRunManager.isCurrentThreadScript();
   }
}
