package org.eclipse.osee.ote.core.framework.command;

public enum RunTestsKeys {
   distributionStatement,
   executablePath,
   classpath,
   version_repositoryType,
   version_location,
   version_revision,
   version_lastAuthor,
   version_lastModificationDate,
   version_modifiedFlag,
   serverOutfilePath,
   executableOutputPath,
   executableDebug,
   testClass,
   clientOutfilePath,
   testVersion,
   serverOutfileFolderOverride,
   executablePathsArray,
   executableArg1Array,
   executableArg2Array,
   spare1,
   batchFailAbortMode,
   batchmode,
   batchFailPauseMode,
   printFailToConsoleMode;
   
}
