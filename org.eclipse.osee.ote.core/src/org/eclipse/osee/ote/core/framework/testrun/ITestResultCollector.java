/*********************************************************************
 * Copyright (c) 2004, 2007 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.osee.ote.core.framework.testrun;

import org.eclipse.osee.framework.jdk.core.type.IPropertyStore;
import org.eclipse.osee.ote.core.environment.TestEnvironment;

/**
 * @author Roberto E. Escobar
 */
public interface ITestResultCollector {

   public void initialize(IPropertyStore propertyStore, TestEnvironment testEnvironment) throws Exception;

   public void dispose(TestEnvironment testEnvironment) throws Exception;

}
