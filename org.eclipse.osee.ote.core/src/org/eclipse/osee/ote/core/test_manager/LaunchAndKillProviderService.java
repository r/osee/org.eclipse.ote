/*********************************************************************
 * Copyright (c) 2019 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.osee.ote.core.test_manager;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.osee.framework.core.executor.ExecutorAdmin;
import org.eclipse.osee.ote.core.test_manager.interfaces.ILaunchAndKillProvider;
import org.eclipse.osee.ote.core.test_manager.interfaces.ILaunchAndKillProviderService;
import org.osgi.framework.FrameworkUtil;

/**
 * @author Dominic Guss
 */
public class LaunchAndKillProviderService implements ILaunchAndKillProviderService {

   private final Collection<ILaunchAndKillProvider> launchAndKillProviders = new ArrayList<>();
   private ExecutorAdmin executorAdmin;

   public LaunchAndKillProviderService() {
      // Register the service
      FrameworkUtil.getBundle(getClass()).getBundleContext().registerService(LaunchAndKillProviderService.class, this,
            null);
   }

   @Override
   public void addLaunchAndKillProvider(ILaunchAndKillProvider launchAndKillProvider) {
      launchAndKillProviders.add(launchAndKillProvider);
      launchAndKillProvider.setExecutorAdmin(this.executorAdmin);
   }

   @Override
   public Collection<ILaunchAndKillProvider> getLaunchAndKillProviders() {
      return launchAndKillProviders;
   }

   @Override
   public void setExecutorAdmin(ExecutorAdmin executorAdmin) {
      this.executorAdmin = executorAdmin;
   }

   @Override
   public ExecutorAdmin getExecutorAdmin() {
      return executorAdmin;
   }
}
