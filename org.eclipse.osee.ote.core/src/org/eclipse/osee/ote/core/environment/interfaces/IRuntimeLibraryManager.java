/*********************************************************************
 * Copyright (c) 2004, 2007 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.osee.ote.core.environment.interfaces;

import org.eclipse.osee.framework.jdk.core.persistence.XmlizableStream;
import org.eclipse.osee.ote.Configuration;
import org.eclipse.osee.ote.ConfigurationStatus;
import org.eclipse.osee.ote.OTEStatusCallback;

/**
 * An interface for loading and installing bundles in the running test environment.
 * 
 * @author Andrew M. Finkbeiner
 *
 */
public interface IRuntimeLibraryManager extends XmlizableStream {
   
   ClassLoader getClassLoader();

   Class<?> loadFromScriptClassLoader(String path) throws ClassNotFoundException;

   Class<?> loadFromRuntimeLibraryLoader(String path) throws ClassNotFoundException;

   boolean installed();

   boolean uninstall(OTEStatusCallback<ConfigurationStatus> callable);

   boolean install(Configuration configuration, OTEStatusCallback<ConfigurationStatus> callable);

   boolean start(OTEStatusCallback<ConfigurationStatus> callable);

   void clearJarCache();

   boolean acquireBundles(Configuration configuration, OTEStatusCallback<ConfigurationStatus> callable);

   void resetScriptLoader(Configuration configuration, String[] strings) throws Exception;

}
