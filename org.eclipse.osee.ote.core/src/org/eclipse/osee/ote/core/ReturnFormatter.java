/*********************************************************************
 * Copyright (c) 2004, 2007 Boeing
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Boeing - initial API and implementation
 **********************************************************************/

package org.eclipse.osee.ote.core;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.eclipse.osee.framework.jdk.core.persistence.Xmlizable;
import org.eclipse.osee.framework.jdk.core.persistence.XmlizableStream;
import org.eclipse.osee.framework.jdk.core.util.xml.Jaxp;
import org.eclipse.osee.framework.jdk.core.util.xml.XMLStreamWriterUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Charles Shaw
 * @author Robert A. Fisher
 */
public class ReturnFormatter implements Xmlizable, XmlizableStream {

    private String returnValue;

    public void set(float value) {
        returnValue = Double.toString(value);
    }

    public void add(double value) {
        returnValue = Double.toString(value);
    }

    public void add(byte value) {
        returnValue = Double.toString(value);
    }

    public void add(short value) {
        returnValue = Double.toString(value);
    }

    public void add(int value) {
        returnValue = Integer.toString(value);
    }

    public void add(long value) {
        returnValue = Double.toString(value);
    }

    public void add(char value) {
        returnValue = Integer.toString(value);
    }

    public void add(boolean value) {
        returnValue = Boolean.toString(value);
    }

    public void add(Object value) {
        returnValue = value == null ? "null" : value.toString();
    }

    @Override
    public String toString() {
        return returnValue;
    }

    @Override
    public Element toXml(Document doc) {
        return Jaxp.createElement(doc, "ReturnValue", returnValue);
    }

    @Override
    public void toXml(XMLStreamWriter writer) throws XMLStreamException {
    	if (returnValue != null) {            
            XMLStreamWriterUtil.writeElement(writer, "ReturnValue", XmlSupport.format(returnValue));    		
    	}
    }

    public String getValue() {
        return returnValue;
    }
    
    @JsonProperty
    public String getReturnValue() {
        return returnValue;
    }
}
