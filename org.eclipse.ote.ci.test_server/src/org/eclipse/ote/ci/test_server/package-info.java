/**
 * Provides the classes necessary to create a test server product that can run OTE test scripts compiled into a product.
 */
package org.eclipse.ote.ci.test_server;